# Kubernetes

Kubernetes Cluster configuration and services documentation, with example source files.

## Documentation TOC

1. [Installation](docs/installation/README.md)
   * [Server Installation](docs/installation/README.md#server-installation)
   * [Host Configuration and Kubernetes Cluster Installation](docs/installation/README.md#host-configuration-and-kubernetes-cluster-installation)
   * [Login with username and password](docs/installation/README.md#login-with-username-and-password)
   * [Create dashboard user and retrieve access token](docs/installation/README.md#create-dashboard-user-and-retrieve-access-token)

   a. [Cluster Upgrading](docs/installation/upgrading.md)

2. [Configuration](docs/configuration/README.md)
   * [Install Helm](docs/configuration/README.md#install-helm)
   * [Install NFS-Client](docs/configuration/README.md#install-nfs-client)
   * [Install MetalLB](docs/configuration/README.md#install-metallb)
   * [Install Consul](docs/configuration/README.md#install-consul)
   * ~~[Install Traefik](docs/configuration/README.md#install-traefik)~~
   * [Install Nginx-Ingress](docs/configuration/README.md#install-nginx-ingress)
   * [Install Cert-Manager](docs/configuration/README.md#install-cert-manager)
      * [Create Production Issuer](docs/configuration/README.md#create-production-issuer)
      * [Create Cloudflare API Key Secret](docs/configuration/README.md#create-cloudflare-api-key-secret)
      * [Create Default Certificate](docs/configuration/README.md#create-default-certificate)
      * [Create ingress for Consul and Dashboard](docs/configuration/README.md#create-ingress-for-consul-and-dashboard)
   * [Install Prometheus-Operator](docs/configuration/README.md#install-prometheus-operator)
   * [Install Weave-Scope](docs/configuration/README.md#install-weave-scope)
   * [Next Steps](docs/configuration/README.md#next-steps)

3. [Services](docs/services/README.md)
   * [Alertmanager](docs/services/README.md#alertmanager)
   * [Ansible/AWX](docs/services/README.md#ansibleawx)
   * [Atlassian Confluence](docs/services/README.md#atlassian-confluence)
   * [Atlassian Jira](docs/services/README.md#atlassian-jira)
   * [FreeIPA](docs/services/README.md#freeipa)
   * [Grafana](docs/services/README.md#grafana)
   * [Guacamole](docs/services/README.md#guacamole)
   * [Home Assistant](docs/services/README.md#home-assistant)
   * [Keycloak](docs/services/README.md#keycloak)
   * [MailDev](docs/services/README.md#maildev)
   * [Matomo](docs/services/README.md#matomo)
   * [Minecraft](docs/services/README.md#minecraft)
   * [Nextcloud](docs/services/README.md#nextcloud)
   * [Node-RED](docs/services/README.md#node-red)
   * [OAuth2-Proxy](docs/services/README.md#oauth2-proxy)
   * [Pi-hole](docs/services/README.md#pi-hole)
   * [Plex](docs/services/README.md#plex)
   * [PostgreSQL](docs/services/README.md#postgresql)
   * [Prometheus-Operator](docs/services/README.md#prometheus-operator)
   * [Next Steps](docs/services/README.md#next-steps)
        * [Current Services](docs/services/README.md#current-services-in-lab)
        * [New Services](docs/services/README.md#new-services)


[redeployment](docs/configuration/redeployment.md) contains ideas for redeploying Kubernetes. This is being worked on in branch v2.

