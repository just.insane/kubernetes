# Services

## Table of Contents

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Services](#services)
  - [Table of Contents](#table-of-contents)
  - [Alertmanager](#alertmanager)
  - [Ansible/AWX](#ansibleawx)
  - [Atlassian Confluence](#atlassian-confluence)
  - [Atlassian Jira](#atlassian-jira)
  - [FreeIPA](#freeipa)
  - [Grafana](#grafana)
  - [Guacamole](#guacamole)
  - [Home Assistant](#home-assistant)
  - [Keycloak](#keycloak)
  - [MailDev](#maildev)
  - [Matomo](#matomo)
  - [Minecraft](#minecraft)
  - [Nextcloud](#nextcloud)
  - [Node-RED](#node-red)
  - [OAuth2-Proxy](#oauth2-proxy)
  - [Pi-hole](#pi-hole)
  - [Plex](#plex)
  - [PostgreSQL](#postgresql)
  - [Prometheus-Operator](#prometheus-operator)
  - [Next Steps](#next-steps)
    - [Current Services (in lab)](#current-services-in-lab)
    - [New Services](#new-services)

<!-- /code_chunk_output -->

## Alertmanager

**NOTE:** Docs not yet completed

Setup via Prometheus-Operator

## Ansible/AWX

**NOTE:** Does not use the built-in PostgreSQL container or Ingress
  * Depends on external postgres database
  * Hardcoded reference to AWX v3.0.0, as there is no docker image for v3.0.1 (latest)

1. Review the [Kubernetes deployment guide](https://github.com/ansible/awx/blob/devel/INSTALL.md#kubernetes)

   * Have `kubectl` and `helm` configured
   * Setup the inventory file
   * To have the playbook setup the postgresql container, ensure `helm` is configured

2. Run the playbook

   * `ansible-playbook -i inventory install.yml`

3. Review the pods and ingress

   * `kubectl get pods --namespace awx` and `kubectl get svc --namespace awx`
   * Check the ingress `kubectl get ing --namespace awx`

4. Create an [ingress](../../src/services/AWX/ingress.yaml)

   * `kubectl create -f ingress.yaml`

## Atlassian Confluence

Overview of steps is here: [https://itnext.io/jira-on-kubernetes-by-helm-8a38357da4e](https://itnext.io/jira-on-kubernetes-by-helm-8a38357da4e)

1. Clone the repository

   * `git clone https://github.com/int128/devops-kompose && cd devops-kompose`

2. Review the [values.yaml](../../src/services/confluence/values.yaml) file.

   * Specific settings that might need to be changed is the proxy port, and hostname.

3. Deploy the Helm Chart (changing the path of values.yaml to the location of the repository)

   * `helm install ./ --name atlassian-confluence -f values.yaml`

4. Setup Confluence via the web interface, including specifying a database (see [PostgreSQL](#postgresql))

* Note: It appears that creating application links between Jira and Confluence is not working, and it's not possible to setup Confluence to use Jira as user store. See #38

## Atlassian Jira

Overview of steps is here: [https://itnext.io/jira-on-kubernetes-by-helm-8a38357da4e](https://itnext.io/jira-on-kubernetes-by-helm-8a38357da4e)

1. Clone the repository

   * `git clone https://github.com/int128/devops-kompose && cd devops-kompose`

2. Review the [values.yaml](../../src/services/jira/values.yaml) file.

   * Specific settings that might need to be changed is the proxy port, and hostname.

3. Deploy the Helm Chart (changing the path of values.yaml to the location of the repository)

   * `helm install ./ --name atlassian-jira -f values.yaml`

4. Setup Jira via the web interface, including specifying a database (see [PostgreSQL](#postgresql))

## FreeIPA

FreeIPA is setup in a 2 node cluster on VMs running on ESXI.

Setup via an [Ansible playbook](https://github.com/Just-Insane/ansible-freeipa)

## Grafana

**NOTE:** Docs not yet completed

Setup via Prometheus-Operator

## Guacamole

1. Clone the Guacamole Helm chart [https://github.com/Just-Insane/apache-guacamole-helm-chart](https://github.com/Just-Insane/apache-guacamole-helm-chart)
or the [upstream](https://github.com/prabhatsharma/apache-guacamole-helm-chart)

2. Apply any changes to [values.yaml](../../src/services/guacamole/values.yaml)

3. Deploy the helm chart from the `apache-guacamole-helm-chart` directory

    * `helm install . -f values.yaml --name=guacamole --namespace=guacamole`

4. An ingress is created by the helm chart

## Home Assistant

**NOTE:** This does not currently support USB device pass-through (needed for z-wave/zigbee devices). See issue #37.

1. Review the Helm Chart [documentation](https://github.com/helm/charts/tree/master/stable/home-assistant)

2. Review the [values.yaml](../../src/services/home-assistant/values.yaml) file.

3. Deploy the Helm Chart

   * `helm install --name home-assistant -f values.yaml stable/home-assistant`

4. Configuring Home Assistant

   * Per the [Helm Chart docs](https://github.com/helm/charts/tree/master/stable/home-assistant#regarding-configuring-home-assistant) a lot of Home Assistant is configured via the config files stored on persistent storage
   * It is unclear how modifying the config files affects Home Assistant in Kubernetes
   * This Chart includes the [Home Assistant Configurator UI](https://github.com/danielperna84/hass-configurator) which allows you to configure Home Assistant via the web browser. It can be found at [https://hassconfig.corp.justin-tech.com:30443/](https://hassconfig.corp.justin-tech.com:30443/)

5. If there are errors, try setting the DNS settings for the deployment. [wiki](https://gitlab.com/just.insane/kubernetes/-/wikis/troubleshooting/services)

## Keycloak

Further information can be found here: [https://github.com/helm/charts/tree/master/stable/keycloak](https://github.com/helm/charts/tree/master/stable/keycloak) including value definitions and config examples

1. Apply any changes to [values.yaml](../../src/services/keycloak/values.yaml)

2. Deploy the helm chart stable/keycloak with values

   * `helm install --name keycloak stable/keycloak --values values.yaml`

3. Create an [ingress](../../src/services/keycloak/ingress.yaml)

   * `kubectl apply -f ingress.yaml`

4. Get the default password for the `keycloak` user.

   * `kubectl get secret --namespace default keycloak-http -o jsonpath="{.data.password}" | base64 --decode; echo`

## MailDev

**NOTE:** Docs not yet completed

[Helm Chart](https://github.com/Just-Insane/MailDev-Helm)

## Matomo

**NOTE:** Docs not yet completed

## Minecraft

**NOTE:** Docs not yet completed

## Nextcloud

1. Review the Helm Chart [documentation](https://github.com/helm/charts/tree/master/stable/nextcloud)

2. Review the [values.yaml](../../src/services/nextcloud/values.yaml) file.

3. Deploy the Helm Chart

   * `helm install --name nextcloud -f values.yaml stable/nextcloud`

## Node-RED

**NOTE:** Docs not yet completed

## OAuth2-Proxy

**NOTE:** Documentation not yet completed

Using https://github.com/pusher/oauth2_proxy

**Note:** this can be replaced with Istio Gateway and a custom EnvoyFilter See:

1. (https://discuss.istio.io/t/istio-oauth-2-0/668/5)
2. (https://programmaticponderings.com/2019/01/06/securing-kubernetes-withistio-end-user-authentication-using-json-web-tokens-jwt/)
3. (https://medium.com/plangrid-technology/custom-user-authentication-in-istio-67c90458b093)
4. (https://medium.com/@suman_ganta/openid-authentication-with-istio-a32838adb492)
5. (https://stackoverflow.com/questions/55159887/istio-oauth2-with-keycloak)
6. (https://stackoverflow.com/questions/54153841/istio-auth-url-support-in-end-user-authentication)

Alternatively, look into [ORY](https://www.ory.sh/docs/ecosystem/overview)

## Pi-hole

**NOTE:** Docs not yet completed

## Plex

**NOTE:** If Plex is not already running on the network, step 6 can be ignored, and step 7 can be modified to use the proper URL of the service

**INFO:** Non-critical parts of the [Kube-Plex](https://github.com/munnerz/kube-plex) repo have been removed due to issues with git sub-repos.

1. Setup an NFS storage class for Plex media storage

   * `helm install stable/nfs-client-provisioner --name plex-nfs --set nfs.server=10.0.40.5 --set nfs.path=/mnt/Storage/Media --set storageClass.name=plex-nfs`

2. Review the Helm Chart [documentation](https://github.com/munnerz/kube-plex)

3. Review the [README.md](../../src/services/plex/kube-plex/README.md) from [Kube-Plex](https://github.com/munnerz/kube-plex).

4. Review the [values.yaml](../../src/services/plex/kube-plex/charts/kube-plex/values.yaml)

5. Install Plex

   * `helm install ./src/services/plex/kube-plex/charts/kube-plex --name plex --namespace plex --set claimToken=<claim_token> --values /src/services/plex/kube-plex/charts/kube-plex/values.yaml`

6. Since plex is already running on the network, port-forward localhost to the new plex server

   * `kubectl port-forward pod/<pod-name> 5000:32400 -n plex`

7. Connect to [http://localhost:5000/web](http://localhost:5000/web) to access and setup Plex

## PostgreSQL

**NOTE:** This could be deployed as a central database cluster for multiple services (current lab setup), or as a database server per service.

1. Review the [Bitnami documentation](https://github.com/bitnami/charts/tree/master/upstreamed/postgresql/#installing-the-chart)

2. Install postgres in a production deployment setup (see the values in [values-production.yaml](../../src/services/postgres/values-production.yaml), or in [values.yaml](../../src/services/postgres/values.yaml) for a single host setup)

**NOTE:** The production setup is not currently working, as the database and user is not automatically created. This works fine in the standalone setup however.

3. Install postgres via Helm

   * `helm install --name postgres -f ./values-production.yaml stable/postgresql`

4. Review the deployment notes, for example:

    1. PostgreSQL can be accessed via port 5432 on the following DNS name from within your cluster:

        `postgres-postgresql.default.svc.cluster.local - Read/Write connection`

        `postgres-postgresql-read.default.svc.cluster.local - Read only connection`

    2. To get the password for "postgres" run:

        `export POSTGRES_PASSWORD=$(kubectl get secret --namespace default
        postgres-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)`

    3. To connect to your database run the following command:

        `kubectl run postgres-postgresql-client --rm --tty -i --restart='Never' --namespace default --image bitnami/postgresql --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql --host postgres-postgresql -U postgres`

    4. To connect to your database from outside the cluster execute the following commands:

        `kubectl port-forward --namespace default svc/postgres-postgresql 5432:5432`

        `psql --host 127.0.0.1 -U postgres`

**NOTE:** it is easier to use pgcli on MacOS than PSQL when connecting to create databases

## Prometheus-Operator

**NOTE:** Docs not yet completed

## Next Steps

### Current Services (in lab)

* [ ] 3CX (or other VoIP)
* [ ] AlertManager - Deployed as part of Prometheus-Operator
  * [ ] Documentation
* [X] Ansible/AWX
* [X] Atlassian Confluence
* [X] Atlassian Jira
* [ ] [Bitwarden](https://github.com/mcfedr/bitwarden-chart)
* [ ] Download services - no longer in use
  * [ ] Lidarr
  * [ ] NZBGet
  * [ ] Radarr
  * [ ] Sonarr
* [ ] [Elastic Stack](https://github.com/helm/charts/tree/master/stable/elastic-stack)
  * [ ] Beats
  * [ ] Elasticsearch
  * [ ] Kibana
  * [ ] Logstash
* [X] FreeIPA - Done in VMs since the offical docker container is still beta
* [X] [Grafana](https://github.com/helm/charts/tree/master/stable/grafana) - Deployed as part of Prometheus-Operator
  * [ ] Documentation
* [X] Guacamole
* [ ] ~~HaProxy~~ - not needed
* [X] Home Assistant
* [ ] [Jupyter](https://zero-to-jupyterhub.readthedocs.io/en/v0.4-doc/setup-jupyterhub.html#setup-jupyterhub)
  * [ ] [JupyterHub](https://github.com/jupyterhub/zero-to-jupyterhub-k8s/blob/master/jupyterhub/values.yaml)
* [X] Keycloak
* [X] [MailDev](https://github.com/Just-Insane/MailDev-Helm)
* [X] [Matomo](https://github.com/jptissot/matomo-chart)
  * [ ] Documentation
* [ ] MayanEDMS
* [X] [Minecraft](https://github.com/helm/charts/tree/master/stable/minecraft)
  * [ ] Documentation
* [X] Nextcloud
* [X] [Node-RED](https://github.com/helm/charts/tree/master/stable/node-red)
  * [ ] Documentation
* [X] OAuth2-Proxy
  * [ ] Documentation
* [X] [PiHole](https://github.com/ChrisPhillips-cminion/pihole-helm)
  * [ ] Documentation
* [X] [Plex](https://github.com/munnerz/kube-plex) - Seems to lack support for newer Plex versions
* [X] PostgreSQL (if needed)
* [X] [Prometheus-Operator](https://github.com/helm/charts/tree/master/stable/prometheus-operator)
  * [ ] Documentation
* [ ] Vault (HashiCorp) (https://github.com/PremiereGlobal/vault-helm-chart)
* [ ] Zabbix

### New Services

* [ ] NZBHydra2
* [X] [Prometheus](https://github.com/helm/charts/tree/master/stable/prometheus) - As part of Prometheus-Operator
  * [ ] Documentation
* [ ] Eclipse Che
* [ ] Sourcegraph
* [ ] [MonicaHQ](https://www.monicahq.com/)
* [ ] [InvoiceNinja](https://www.invoiceninja.org/)
* [ ] [Bitwarden_rs](https://github.com/dani-garcia/bitwarden_rs) (as a replacement for the offical Bitwarden server)
* [ ] Wiki (in place of Confluence?)
* [ ] [Bank-Vaults](https://github.com/banzaicloud/bank-vaults) (Manage HashiCorp Vault and provide secrets to containers. See #33)