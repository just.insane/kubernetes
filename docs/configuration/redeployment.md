# Redeployment

## After Installation

1. Install Helm
2. Install NFS-Client
3. Install MetalLB
4. Install Cert-Manager (long lived certificates & Let's Encrypt)
5. Install [Prometheus-Operator](https://github.com/helm/charts/tree/master/stable/prometheus-operator)
    1. Prometheus
    2. Alertmanager
    3. node-exporter
    4. kube-state-metrics
    5. grafana
    6. scraping for Kubernetes cluster
6. Install [Bank-Vaults](https://github.com/banzaicloud/bank-vaults)
    1. Install [Consul](https://github.com/hashicorp/consul-helm)
       1. Not using Connect
       2. Not using MeshGateway
       3. Purely K/V store for Vault
7. Install [Elastic-Cloud-Operator](https://www.elastic.co/elasticsearch-kubernetes)
    1. Elasticsearch
    2. Kibana
8. Install [Logging-Operator](https://github.com/banzaicloud/logging-operator)
    1. Fluentd
    2. Fluentbit
    **NOTE:** To get logging, you can add the config via [Helm](https://github.com/banzaicloud/logging-operator/blob/master/charts/nginx-logging-demo/templates/logging.yaml)
9.  Install [Istio](https://istio.io/docs/setup/install/helm/) or [Istio-Operator](https://github.com/banzaicloud/istio-operator)
    1. Install Tracing
       1. [Jaeger-Operator](https://github.com/helm/charts/tree/master/stable/jaeger-operator)
       2. Deploy all-in-one as trace history needed needed
          1. See trace history issue with dependencies.
             1. [Docs](https://www.jaegertracing.io/docs/1.14/operator/#elasticsearch-storage)
             2. [Github Issue](https://github.com/jaegertracing/jaeger-operator/issues/294)
    2. Install [Kiali](https://www.kiali.io/documentation/getting-started/#_install_kiali_latest)
    3. Grafana via Prometheus-Operator
       1. Do not automatically deploy dashboards, import the JSON manually for best experience
    4. Prometheus via Prometheus-Operator
    5. Fluentd via Logging-Operator
10. Install [Anchore Image Validator](https://github.com/banzaicloud/anchore-image-validator) - pod security
    1. Install Anchore Engine
11. Install Keycloak (install stage might change depending on OIDC use in above items)
12. Install OAuth2-Proxy
    1.  Allows authorizing and authenticating ingress traffic into the environment

## Potential apps

1.  Install Gitlab-CE
    1.  CI/CD environment
    2.  [Helm Chart Repository](https://tobiasmaier.info/posts/2018/03/13/hosting-helm-repo-on-gitlab-pages.html)
        1.  Alternatively, look at [ChartMuseum](https://github.com/helm/chartmuseum)
    3.  Docker Repository
2.  [Sonarqube](https://github.com/banzaicloud/banzai-charts/tree/master/sonarqube)
3.  [Chaoskube](https://github.com/helm/charts/tree/master/stable/chaoskube)
4.  [ClamAV](https://github.com/helm/charts/tree/master/stable/clamav)
5.  [Elastalert](https://github.com/helm/charts/tree/master/stable/elastalert)
6.  [Falco](https://github.com/helm/charts/tree/master/stable/falco)
7.  [helm-exporter](https://github.com/helm/charts/tree/master/stable/helm-exporter)
8.  [Katafygio](https://github.com/helm/charts/tree/master/stable/katafygio)
9.  [kube-hunter](https://github.com/aquasecurity/kube-hunter)
10. [Kubedb](https://github.com/kubedb/installer/tree/v0.13.0-rc.0/chart/kubedb)
11. [kuberhealthy](https://github.com/helm/charts/tree/master/stable/kuberhealthy)
12. [locust](https://github.com/helm/charts/tree/master/stable/locust)
13. [Minio](https://github.com/helm/charts/tree/master/stable/minio)
14. [openvpn](https://github.com/helm/charts/tree/master/stable/openvpn)
15. [Velero](https://github.com/helm/charts/tree/master/stable/velero)